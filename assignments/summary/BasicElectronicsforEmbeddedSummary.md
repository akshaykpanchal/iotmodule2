# **Basic concepts in Electronics:**

## **Sensors & Actuators:**

### **Sensors:**

> #### *We know that, a transducer is a physical device that converts some form of energy into another form. Well, the sensor is essentially a transducer that converts some form of energy into an electrical signal which upon processing is used to take a reading.*
> #### *For example, lamps which dim or brighten by touching the base, touch-sensitive elevator buttons, etc.*

> * #### *A better term for a sensor is a transducer.*
> * #### *A transducer is any physical device that converts one form of energy into another.*
> * #### *Sensor is, the transducer converts some physical energy into an electrical signal that can then be used to take a reading.*
> * #### *For Example : A microphone is a sensor that takes vibrational energy (sound waves), and converts it to electrical signal for the system to relate it back to the original sound.*


<img src="img/1.png">

### **Actuators:**
> #### *It is another type of a transducer. It works in the reverse direction of a sensor. That is, the Actuator takes an electrical input and turns it into a required physical action. It is also called as a mover.*
> #### *For example, opening a valve upon recieving an electrical signal, a hydraulic system, etc.*

> #### *Another type of transducer is an actuator.*
> #### *An actuator operates in the reverse direction of a sensor.*
> #### *Actuator takes an electrical input and turns it into physical action.*
> #### *For example: an electric motor, a hydraulic system, and a pneumatic system are all different types of actuators.*

<img src="img/2.png">

# **Electrical Signals:**

## **Two main types of electrical signals encountered in practice**
> ### **1. Analog signals**
> ### **2. Digital signals**

#### **Analog Signals:**
> #### *An analog signal is any continuous signal for which the time-varying feature (variable) of the signal is a representation of some other time-varying quantity, i.e., analogous to another time-varying signal.*
> #### *Analog signals contain continuous values, Assume our heartbeat when we run fast then our heartbeat rate increases, when we take rest, it gradually decreases and gets to the average heartbeat rate. Plotting the graph of this will make an analog signal.*

#### *Examples of an Analog signals are:*
> * #### *Human Voice*
> * #### *Thermometer output*
> * #### *Analog Phones* 

#### **Digital Signals:**
> #### *A digital signal is a signal that is being used to represent data as a sequence of discrete values; at any given time it can only take on one of a finite number of values.*
> * #### *Digital signals are discrete in nature, They vary thier states from one state to another state(eg. logic high, logic low ).* 

#### *Examples of Digital signals are:*
> * #### *Computers*
> * #### *Digital phones*
> * #### *Binary sequence*

<img src="img/3.png">

# **Microcontrollers vs Microprocessors**

#### **Microprocessors:**
> * #### *Microprocessor has only a CPU inside them in one or few Integrated Circuits.* 
> * #### *Like microcontrollers it does not have RAM, ROM and other peripherals. They are dependent on external circuits of peripherals to work.*
> * #### *Microprocessors are not made for specific task but they are required where tasks are complex and tricky like development of software’s, games and other applications that require high memory and where input and output are not defined.* 
> * #### *Microprocessors may be called as a heart of a computer system.*  

#### **Examples of microprocessor are:**
> * #### *Motorola MC68000*
> * #### *Intel core I3*
> * #### *Atmel AT91M42800A**

#### **Microcontrollers:**
> * #### *It’s like a small computer on a single IC. It contains a processor core, ROM, RAM and I/O pins dedicated to perform various tasks.* 
> * #### *Microcontrollers are generally used in projects and applications that require direct control of user. As it has all the components needed in its single chip, it does not need any external circuits to do its task so microcontrollers are heavily used in embedded systems and major microcontroller manufacturing companies are making them to be used in embedded market.* 
> * #### *A microcontroller can be called the heart of embedded system.* 
> * #### *Sometimes microcontrollers referred to as an embedded controller or microcontroller unit (MCU)*
> * #### *microcontrollers are found in vehicles, robots, office machines, medical devices, mobile radio transceivers, vending machines and home appliances, among other devices.*

#### **Examples of popular microcontrollers:**
> * #### *8051*
> * #### *AVR*
> * #### *PIC series of microcontrollers.*

#### **Difference between microcontrollers and microprocessors:**
> #### *1. Key difference in both of them is presence of external peripheral, where microcontrollers have RAM, ROM, EEPROM embedded in it while we have to use external circuits in case of microprocessors.*
> #### *2. As all the peripheral of microcontroller are on single chip it is compact while microprocessor is bulky.*
> #### *3. Microcontrollers are made by using complementary metal oxide semiconductor technology so they are far cheaper than microprocessors. In addition the applications made with microcontrollers are cheaper because they need lesser external components, while the overall cost of systems made with microprocessors are high because of the high number of external components required for such systems.*
> #### *4. Processing speed of microcontrollers is about 8 MHz to 50 MHz, but in contrary processing speed of general microprocessors is above 1 GHz so it works much faster than microcontrollers.*

<img src="img/4.png">

# **Raspberry Pi:**

#### **What is Raspberry pi?** 
> #### *The Raspberry Pi is a low cost, credit-card sized computer that plugs into a computer monitor or TV, and uses a standard keyboard and mouse. It is a capable little device that enables people of all ages to explore computing, and to learn how to program in languages like Scratch and Python.*

#### **Characteristics:**
> * ####  *Mini Computer.*
> * ####  *Limited but large power for its size.*
> * ####  *No storage.*
> * ####  *SoC (System on Chip).*
> * ####  *Can connect Shields.*
> * ####  *Can connect multiple Pi’s together.*
> * ####  *Microprocessor.*
> * ####  *Can load a Linux OS on it.*
> * ####  *Uses ARM architecture.*
> * ####  *Connect to sensors or actuators using Interfaces (GPIO, UART, SPI, I2C, PWM, etc.).*

#### **Features of Raspberry Pi 3:**
> * #### *CPU: Quad-core 64-bit ARM Cortex A53 clocked at 1.2 GHz*
> * #### *GPU: 400MHz VideoCore IV multimedia*
> * #### *Memory: 1GB LPDDR2-900 SDRAM (i.e. 900MHz)*
> * #### *USB ports: 4*
> * #### *Video outputs: HDMI, composite video (PAL and NTSC) via 3.5 mm jack*
> * #### *Network: 10/100Mbps Ethernet and 802.11n Wireless LAN*
> * #### *Peripherals: 17 GPIO plus specific functions, and HAT ID bus*
> * #### *Bluetooth: 4.1*
> * #### *Power source: 5 V via MicroUSB or GPIO *
> * #### *Size: 85.60mm × 56.5mm*
> * #### *Weight: 45g (1.6 oz)*

<img src="img/6.png">

# **Interfaces:**

## **Serial vs Parallel Interfaces:**

#### **Serial Interfaces:**
> * #### *A serial interface is a communication interface that transmits data as a single stream of bits, typically using a wire-plus-ground cable, a single wireless channel or a wire-pair.*
> * #### *The serial interface acts as a communication interface between two digital systems that sends data as a series of voltage pulses over a wire. In contrast, a parallel interface transmits multiple bits simultaneously using different wires.*
> * #### *Some devices that use the serial interface include the Universal Serial Bus (USB), Recommended Standard No. 232 (RS-232), 1-Wire and I2C.*

#### **Examples:**
> * #### *I2C*
> * #### *SPI*
> * #### *UART*

#### **Parallel Interfaces:**
>*  #### *Uses multiple wires running parallel to each other, and can transmit data on all the wires simultaneously.* 
> * #### *Parallel interface refers to a multiline channel, with each line capable of transmitting several bits of data simultaneously.* 
> * #### *Before USB ports became common, most personal computers (PCs) had at least one parallel interface for connecting a printer using a parallel port.* 

#### **Examples:**
> * #### *GPIO* 
> * #### *Parallel port*

<img src="img/7.png">
