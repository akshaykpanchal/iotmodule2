# **Board Electrical Safety instructions**

## **Safety Precautions while using devices:**

## **Do's and Don'ts:**

### **1.Power Supply:**

> #### **Do's:**
> * #### *Always make sure that both the output and input voltages of the board are same.*
> * #### *Before turning on the power supply make sure connections are made properly.*

> #### **Don'ts:**
> * #### *Do not connect power supply without matching the power rating.*
> * #### *Never connect a higher output(12V/3Amp) to a lower (5V/2Amp) input.*
> * #### *Don't connect the wires forcefully into the board.*

### **2.Handling:**

> #### **Do's:**
> * #### *Handle devices safely and make sure your hands are dry.*
> * #### *If the board becomes too hot try to cool it with a external usb fan.*
> * #### *Keep all electrical circuit contact points enclosed.*

> ### **Don'ts:**
> * #### *Don’t handle the board when its powered ON.*
> * #### *Never touch electrical equipment when any part of your body is wet.*
> * #### *Do not touch any sort of metal to the development board.*

### **3.GPIO:**

> #### **Do's:**
> * #### *Find out whether the board runs on 3.3v or 5v logic.*
> * #### *Always connect the LED (or sensors) using appropriate resistors.*
> * #### *To Use 5V peripherals with 3.3V use a logic level converter.*

> #### **Don'ts:**
> * #### *Never connect anything greater than 5v to a 3.3v pin.Don't use high voltages.*
> * #### *Avoid making connections when the board is running.*
> * #### *Do not connect a motor directly , use a transistor to drive it.*

<img src=img/gpio.png>

## **Guidelines for using interfaces(UART,I2C,SPI):**

#### **1.UART**
> * #### *Connect Rx device1 to Tx device2, similarly Tx device1 to Rx device2.*
> * #### *If the device1 works on 5v and device2 works at 3.3v then use the level shifting mechanism(voltage divider).*
> * #### *Genrally UART is used to communicate with board through USB to TTL connection.It doesn't require any protection circuit.*
> * #### *But Senor interfacing using UART require a protection circuit.*

<img src=img/uart.png>

### **2.I2C**
> * #### *using I2C interfaces with sensors SDA and SDL lines must be protected.*
> * #### *Protection of these lines is done by using pullup registers on both lines.*
> * #### *If you use the inbuilt pullup registers it doesn't require an external circuit.*
> * #### *If you are using bread-board to connect your sensor,use the pullup resistor(2.2kohm <= 4K ohm).*

<img src=img/i2c.png>

### **3.SPI**
> * #### *SPI in development boards does not require any protection circuit.*
> * #### *when you are using more than one slaves device2 can "hear" and "respond" to the master's communication with device1 which is an disturbance.*
> * #### *To overcome the disturbance,we use a protection circuit with pullup resistors(1kOhm ~10kOhm,Generally 4.7kOhm) on each the Slave Select line(CS).*

<img src=img/spi.png>


