#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <shunyaInterfaces.h>
#include <dirent.h>


//meter connection
RM = opendir("/dev/ttyAMA0");

int main(void)
{
//initialization of shunya interfaces
shunyaInterfacesSetup();

//modbus TCP/IP configuration
//user enter their modbus data
"modbus-tcp":{
"type": "tcp",
"ipaddr": " ",
"port": " "}

//modbus connect read/write data
modbusObj RM = newModbus("modbus-tcp"); // for creating new instance
modbusTcpConnect(&RM); // connect to modbus as per configs

// reading data of different parameters
float ac_current = modbusTcpRead(&RM, 30007);
float voltage = modbusTcpRead(&RM, 30001);
float power = modbusTcpRead(&RM, 30013);
float Active_energy = modbusTcpRead(&RM, 30147);
float Reactive_energy = modbusTcpRead(&RM, 30151);
float Apparent_energy = modbusTcpRead(&RM, 30081);
float THD_of_Voltage = modbusTcpRead(&RM, 30219);

return 0;
}
